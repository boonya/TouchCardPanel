package org.touch.card.vl;

import java.util.ArrayList;
import java.util.List;

import org.touch.card.R;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

public class CardVlAsPanelAty extends FragmentActivity implements
		OnTouchListener {

	private View v1;
	private View v2;
	private View v3;
	private View v4;
	View rootView;
	List<View> lstViews = new ArrayList<View>();

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.root_vl_home);
		rootView = findViewById(R.id.panel_frame);
		v1 = findViewById(R.id.ft_1);
		v2 = findViewById(R.id.ft_2);
		v3 = findViewById(R.id.ft_3);
		v4 = findViewById(R.id.ft_4);

		v1.setOnTouchListener(this);
		v2.setOnTouchListener(this);
		v3.setOnTouchListener(this);
		v4.setOnTouchListener(this);
		setupPanelView();
		lstViews.add(v1);
		lstViews.add(v2);
		lstViews.add(v3);
		lstViews.add(v4);
	}

	static final FrameLayout.LayoutParams fullLayoutParams = new LayoutParams(
			LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);

	void readPanelLayoutParams(View v) {
		FrameLayout.LayoutParams lp = (LayoutParams) v.getLayoutParams();
		v.setTag(R.id.panel_item_top, lp.topMargin);
		v.setTag(R.id.panel_item_bottom, lp.bottomMargin);
	}

	private void setupPanelView() {
		if (v1 != null) {
			readPanelLayoutParams(v1);
			FrameLayout.LayoutParams lp = (LayoutParams) v1.getLayoutParams();
			v1.setTag(lp);
			v1.setTag(R.id.panel_item_is_fullscreen, false);
		}
		if (v2 != null) {
			readPanelLayoutParams(v2);
			FrameLayout.LayoutParams lp = (LayoutParams) v2.getLayoutParams();
			v2.setTag(lp);
			v2.setTag(R.id.panel_item_is_fullscreen, false);
		}
		if (v3 != null) {
			readPanelLayoutParams(v3);
			FrameLayout.LayoutParams lp = (LayoutParams) v3.getLayoutParams();
			v3.setTag(lp);
			v3.setTag(R.id.panel_item_is_fullscreen, false);
		}
		if (v4 != null) {
			FrameLayout.LayoutParams lp = (LayoutParams) v4.getLayoutParams();
			readPanelLayoutParams(v4);
			v4.setTag(lp);
			v4.setTag(R.id.panel_item_is_fullscreen, false);
		}
	}

	int lastY;

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			lastY = (int) event.getRawY();
			break;
		case MotionEvent.ACTION_MOVE:
			int dy = (int) event.getRawY() - lastY;
			onTouchMovePanel(v, dy);
			lastY = (int) event.getRawY();
			break;
		case MotionEvent.ACTION_UP:
			int moveY = (int) event.getRawY();
			OnRohanHoming(v,moveY);
			break;
		}
		return true;
	}

	void OnRohanHoming(View view,int moveY) {
		if (v1 == view) {
			int top = v1.getTop();
			boolean isFull = (Boolean) v1.getTag(R.id.panel_item_is_fullscreen);
			if(top==0){
				if(isFull){
					FrameLayout.LayoutParams lp = (LayoutParams) v1.getTag();
					v1.setLayoutParams(lp);
					v1.setTag(R.id.panel_item_is_fullscreen, false);
				}else{
					v1.setLayoutParams(fullLayoutParams);
					v1.setTag(R.id.panel_item_is_fullscreen, true);
				}
			}else {
				FrameLayout.LayoutParams lp = (LayoutParams) v1.getTag();
				v1.setLayoutParams(lp);
				v1.setTag(R.id.panel_item_is_fullscreen, false);
			}
			
			return;
		}
		if (v2 == view) {
			int top = v2.getTop();
			boolean isFull = (Boolean) v2.getTag(R.id.panel_item_is_fullscreen);
			if(top==0){
				if(isFull){
					FrameLayout.LayoutParams lp = (LayoutParams) v2.getTag();
					v2.setLayoutParams(lp);
					v2.setTag(R.id.panel_item_is_fullscreen, false);
				}else{
					v2.setLayoutParams(fullLayoutParams);
					v2.setTag(R.id.panel_item_is_fullscreen, true);
				}
			}else {
				FrameLayout.LayoutParams lp = (LayoutParams) v2.getTag();
				v2.setLayoutParams(lp);
				v2.setTag(R.id.panel_item_is_fullscreen, false);
			}
			
			return;
		}
		if (v3 == view) {
			FrameLayout.LayoutParams lp = (LayoutParams) v3.getTag();
			v3.setLayoutParams(lp);
			return;
		}
		if (v4 == view) {
			FrameLayout.LayoutParams lp = (LayoutParams) v4.getTag();
			v4.setLayoutParams(lp);
			return;
		}
	}

	void onTouchMovePanel(View view, int moveY) {
		int moveY2 = (int) (moveY >> 1);
		int moveY3 = (int) (moveY2 >> 1);
		int moveY4 = (int) (moveY3 >> 1);
		if (view == v1) {
			int left = v1.getLeft();
			int top = v1.getTop() + moveY;
			int right = v1.getRight();
			int bottom = v1.getBottom() + moveY;
			if(top<0){
				top=0;
			}
			if(bottom<0){
				bottom = 0;
			}
			
			v1.layout(left, top, right, bottom);
			//
			int left2 = v2.getLeft();
			int top2 = v2.getTop() + moveY2;
			int right2 = v2.getRight();
			int bottom2 = v2.getBottom() + moveY2;
			v2.layout(left2, top2, right2, bottom2);
			//
			int left3 = v3.getLeft();
			int top3 = v3.getTop() + moveY3;
			int right3 = v3.getRight();
			int bottom3 = v3.getBottom() + moveY3;
			v3.layout(left3, top3, right3, bottom3);
			//
			int left4 = v4.getLeft();
			int top4 = v4.getTop() + moveY4;
			int right4 = v4.getRight();
			int bottom4 = v4.getBottom() + moveY4;
			v4.layout(left4, top4, right4, bottom4);
			return;
		}
		//
		if (view == v2) {
			int left2 = v2.getLeft();
			int top2 = v2.getTop() + moveY;
			int right2 = v2.getRight();
			int bottom2 = v2.getBottom() + moveY;
			if(top2<0){
				top2=0;
			}
			if(bottom2<0){
				bottom2 = 0;
			}
			// if(top2<150&&v2.getLayoutParams()!=fullLayoutParams){
			// v2.setLayoutParams(fullLayoutParams);
			// }else{
			v2.layout(left2, top2, right2, bottom2);
			// }
			return;
		}
		//
		if (view == v3) {
			int left3 = v3.getLeft();
			int top3 = v3.getTop() + moveY;
			int right3 = v3.getRight();
			int bottom3 = v3.getBottom() + moveY;
			if(top3<0){
				top3=0;
			}
			if(bottom3<0){
				bottom3 = 0;
			}
			v3.layout(left3, top3, right3, bottom3);
			return;
		}
		//
		if (view == v4) {
			int left4 = v4.getLeft();
			int top4 = v4.getTop() + moveY;
			int right4 = v4.getRight();
			int bottom4 = v4.getBottom() + moveY;
			if(top4<0){
				top4=0;
			}
			if(bottom4<0){
				bottom4 = 0;
			}
			v4.layout(left4, top4, right4, bottom4);
			return;
		}

	}

}
