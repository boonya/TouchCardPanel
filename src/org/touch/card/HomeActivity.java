package org.touch.card;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class HomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.root_home);
//		Intent i = new Intent(this,CardGridAsPanelAty.class);
		Intent i = new Intent(this,org.touch.card.vl.CardVlAsPanelAty.class);
//		Intent i = new Intent(this,org.touch.card.vl.v2.CardVlAsPanelAty.class);
		startActivity(i);
	}


}
